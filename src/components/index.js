import TabIcon from './TabIcon';
import Loading from './Loading';
import ListMovies from './ListMovies';
import PosterMovies from './PosterMovies';
import Genres from './Genres';
import IconButton from './IconButton';
import FloatMovie from './FloatMovie';
import Card from './Card';
import NoInternet from './NoInternet';

export {TabIcon, Loading, ListMovies, PosterMovies, Genres, IconButton, FloatMovie, Card, NoInternet};
