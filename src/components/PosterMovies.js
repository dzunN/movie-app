import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {SIZES} from '../constants';

const PosterMovies = ({poster, onPress}) => {
  return (
    <>
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.container}>
          {/* Thumbnail */}
          <ImageBackground
            source={{uri: poster}}
            style={styles.poster_img}
            imageStyle={styles.img_style}
          />
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

export default PosterMovies;

const styles = StyleSheet.create({
  container: {
    width: SIZES.width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  poster_img: {
    width: SIZES.width * 0.9,
    height: SIZES.height * 0.6,
    justifyContent: 'flex-end',
  },
  img_style: {
    borderRadius: 18,
  },
});
