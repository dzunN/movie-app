/* eslint-disable react-native/no-inline-styles */
import {
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../constants';
import {Rating} from 'react-native-elements';
import Genres from './Genres';
import Axios from '../services';

const ListMovie = ({item, onPress}) => {
  const [genres, setGenres] = React.useState([]);
  React.useEffect(() => {
    Axios.get(`/movies/${item.id}`).then(res => {
      setGenres(res.data.genres);
    });
  }, [item.id]);
  function renderGenre() {
    return (
      <>
        <View style={styles.SecGenre}>
          <Text style={styles.itemGenre}>{genres[0]?.name}</Text>
        </View>
        {genres?.length < 2 ? null : (
          <View style={styles.SecGenre}>
            <Text style={styles.itemGenre}>{genres[1]?.name}</Text>
          </View>
        )}
        {genres?.length > 2 ? (
          <View style={styles.SecGenre}>
            <Text style={styles.itemGenre}>{genres?.length - 2}+</Text>
          </View>
        ) : null}
      </>
    );
  }
  return (
    <>
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.container}>
          <Image source={{uri: item?.poster_path}} style={styles.img_poster} />
          <View style={styles.info}>
            <Text style={styles.titleFilm}>{item?.title}</Text>
            <Text style={styles.release_date}>{item?.release_date}</Text>
            <View style={styles.genres}>{renderGenre()}</View>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Rating
                imageSize={20}
                readonly
                startingValue={item?.vote_average / 2}
                ratingCount={5}
                fractions={2}
                style={styles.rating}
                tintColor={COLORS.black}
              />
              <Text style={styles.count_rating}>{item?.vote_average}</Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

export default ListMovie;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: SIZES.padding,
    // width: SIZES.width * 0.9,
    height: SIZES.height * 0.2,
  },
  img_poster: {
    width: null,
    height: null,
    borderRadius: 11,
    marginRight: 16,
    flex: 2,
  },
  info: {
    flex: 4,
  },
  titleFilm: {
    ...FONTS.h2,
    color: COLORS.white,
    marginBottom: 10,
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  release_date: {
    ...FONTS.body4,
    color: COLORS.gray,
    marginBottom: 10,
  },
  rating: {
    alignSelf: 'flex-start',
  },
  count_rating: {
    marginLeft: 4,
    color: COLORS.lightGray,
  },
  genres: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  SecGenre: {
    backgroundColor: COLORS.gray1,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginRight: 7,
    paddingHorizontal: 7,
  },
  itemGenre: {
    ...FONTS.body4,
    color: COLORS.white,
    textAlign: 'center',
  },
});
