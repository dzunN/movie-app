import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import React from 'react';
import {FONTS, COLORS} from '../constants';

function Loading() {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="light-content" backgroundColor={COLORS.black} />
      <ActivityIndicator size="large" color={COLORS.primary} />
      {/* <Text style={styles.text}>Loading ...</Text> */}
    </View>
  );
}

export default Loading;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: COLORS.black,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  text: {
    ...FONTS.h2,
    color: COLORS.primary,
    marginTop: 12,
  },
});
