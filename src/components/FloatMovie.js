import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Rating} from 'react-native-elements';
import {COLORS, FONTS, SIZES} from '../constants';

const FloatMovie = ({
  title,
  tagline,
  status,
  date,
  voteAverage,
  runtime,
  source,
}) => {
  return (
    <View style={styles.card}>
      <Image source={source} style={styles.image} />
      <View style={styles.detailFilm}>
        <Text style={styles.title}>{title}</Text>
        <View style={{flexDirection: 'row', marginTop: 5}}>
          <Text style={styles.description}>Tagline : </Text>
          <Text style={styles.contentDescription}>{tagline}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.description}>Status : </Text>
          <Text style={styles.contentDescription}>{status}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.description}>Release Date : </Text>
          <Text style={styles.contentDescription}>{date}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.description}>Runtime : </Text>
          <Text style={styles.contentDescription}>{runtime} minute</Text>
        </View>
        <Text style={styles.ratingNumber}>{voteAverage}/10</Text>
        {/* <Rating
          imageSize={20}
          readonly
          startingValue={voteAverage / 2}
          ratingCount={5}
          fractions={2}
          style={styles.rating}
          tintColor={COLORS.black}
        /> */}
      </View>
    </View>
  );
};

export default FloatMovie;

const styles = StyleSheet.create({
  card: {
    width: SIZES.width * 0.95,
    height: SIZES.height * 0.3,
    backgroundColor: COLORS.black,
    alignSelf: 'center',
    marginTop: -SIZES.height * 0.16,
    borderRadius: 10,
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexDirection: 'row',
  },

  image: {
    height: '90%',
    width: null,
    flex: 2,
    marginRight: 10,
    borderRadius: 10
  },

  detailFilm: {
    flex: 4,
  },

  title: {
    ...FONTS.h2,
    color: COLORS.white,
  },

  description: {
    ...FONTS.h4,
    color: COLORS.lightGray,
  },

  contentDescription: {
    ...FONTS.h4,
    color: COLORS.lightGray,
  },

  rating: {
    alignSelf: 'flex-start',
  },

  ratingNumber: {
    ...FONTS.h4,
    color: COLORS.lightGray,
    marginTop: 5,
  },
});
