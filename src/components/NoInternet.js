import {StatusBar, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS} from '../constants';

const NoInternet = () => {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="light-content" backgroundColor={COLORS.black} />
      <Text style={styles.title}>You're Offline</Text>
      <Text style={styles.text}>
        Check your internet connection and try again
      </Text>
    </View>
  );
};

export default NoInternet;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: COLORS.black,
    width: '100%',
    height: '100%',
    paddingHorizontal: 40,
    justifyContent: 'center',
  },
  title: {
    ...FONTS.h1,
    color: COLORS.primary,
    marginTop: 12,
  },
  text: {
    ...FONTS.body3,
    textAlign: 'center',
    color: COLORS.white,
    marginTop: 12,
  },
});
