import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {icons, COLORS} from '../constants';

const IconButton = ({type, onPress}) => {
  const Icon = () => {
    if (type === 'Back') return <icons.ICBack />;
    if (type === 'Like') return <icons.ICLike />;
    if (type === 'Share') return <icons.ICShare />;

    return <icons.ICBack />;
  };

  return (
    <TouchableOpacity style={styles.iconWrapper} onPress={onPress}>
      <View style={styles.icon}>
        <Icon />
      </View>
    </TouchableOpacity>
  );
};

export default IconButton;

const styles = StyleSheet.create({
  iconWrapper: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: COLORS.transparentDarkGray,
    opacity: 0.8,
  },
  icon: {
    padding: 16,
    alignSelf: 'center',
    borderRadius: 8,
  },
});
