import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {COLORS, FONTS, SIZES} from '../constants';

const Genres = ({genreName}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.item}>{genreName}</Text>
    </View>
  );
};

export default Genres;

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.gray1,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginRight: 7,
    paddingHorizontal: 7,
  },
  item: {
    ...FONTS.body4,
    color: COLORS.white,
    textAlign: 'center',
  },
});
