import ICBack from '../assets/icons/back.svg';
import ICDownload from '../assets/icons/download.svg';
import ICLike from '../assets/icons/like.svg';
import ICShare from '../assets/icons/share.svg';

export default {
  ICBack,
  ICShare,
  ICLike,
  ICDownload,
};
