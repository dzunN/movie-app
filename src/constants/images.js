import Linier_gradient from "../assets/images/bgLinier_Gradient.svg";
const profile_photo = require("../assets/images/dummy_profile/profile_photo.png");
const Linier_png = require("../assets/images/bgLinier_Gradient.svg");

export default {
    profile_photo,
    Linier_gradient,
    Linier_png,
}