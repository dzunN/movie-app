import React from 'react';
import LottieView from 'lottie-react-native';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {COLORS, FONTS} from '../constants';

function Splash({navigation}) {
  React.useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 3000);
  });
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="light-content" backgroundColor={COLORS.black} />
      <LottieView
        source={require('../assets/images/animasi.json')}
        autoPlay
        loop={false}
        speed={0.6}
        onAnimationFinish={() => {
          navigation.replace('Home');
        }}
      />
      <Text style={styles.nickname}>Dzun Nurroin</Text>
    </View>
  );
}

export default Splash;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: COLORS.black,
    justifyContent: 'center',
  },
  nickname: {
    ...FONTS.body3,
    color: COLORS.lightGray,
    position: 'absolute',
    bottom: 19,
  },
});
