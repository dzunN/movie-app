import Home from './Home';
import MovieDetail from './MovieDetail';
import Splash from './Splash';

export {Home, MovieDetail, Splash};
