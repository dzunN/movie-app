import React, {useState, useEffect} from 'react';
import {
  Animated,
  FlatList,
  Image,
  ImageBackground,
  RefreshControl,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Share from 'react-native-share';
import {Card, FloatMovie, Genres, IconButton, Loading} from '../components';
import {COLORS, FONTS, SIZES} from '../constants';
import Axios from '../services';

const MovieDetail = ({route, navigation}) => {
  const [data, setData] = useState([]);
  const {id} = route.params;
  const [isLoading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  let isMounted = true;

  function onRefresh() {
    setRefreshing(true);
    getData(id);
  }

  async function ShareContent() {
    const shareOptions = {
      message: `${data.original_title} ${data.homepage}`,
    };

    try {
      await Share.open(shareOptions);
    } catch (error) {
      console.log('Error : ', error);
    }
  }

  const setFalse = () => {
    setLoading(false);
    setRefreshing(false);
  };

  async function getData(id) {
    try {
      const response = await Axios.get(`/movies/${id}`);
      if (isMounted) setData(response.data);
    } catch (error) {
      console.error(error);
      setFalse();
    } finally {
      setFalse();
    }
  }

  useEffect(() => {
    getData(id);
    return () => {
      isMounted = false;
    };
  }, []);

  if (!data) {
    return null;
  }

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <View style={styles.container}>
          <StatusBar barStyle="light-content" backgroundColor={COLORS.black} />
          <ScrollView
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }>
            <ImageBackground
              source={{uri: data.backdrop_path}}
              style={styles.header}
              imageStyle={{opacity: 0.5}}>
              <View style={styles.iconButton}>
                <View style={{flex: 3}}>
                  <IconButton type="Back" onPress={() => navigation.goBack()} />
                </View>
                <View style={styles.icons2}>
                  <View style={{paddingLeft: 40}}>
                    <IconButton type="Like" />
                  </View>
                  <View>
                    <IconButton type="Share" />
                  </View>
                </View>
              </View>
            </ImageBackground>
            <FloatMovie
              title={data.title}
              voteAverage={data.vote_average}
              status={data.status}
              date={data.release_date}
              runtime={data.runtime}
              tagline={data.tagline}
              source={{uri: data.poster_path}}
            />
            <View style={styles.SecSection}>
              <Text style={styles.label}>Genres</Text>
              <View style={styles.secGenre}>
                {data.genres?.map(item => {
                  return <Genres key={item.id} genreName={item.name} />;
                })}
              </View>
              <Text style={styles.label}>Synopsis</Text>
              <View style={styles.SecSynopsis}>
                <Text style={styles.synopsis}>{data.overview}</Text>
              </View>
              <Text style={styles.label}>Actor/Artist</Text>
              <FlatList
                data={data.credits?.cast}
                numColumns={3}
                columnWrapperStyle={{justifyContent: 'space-around'}}
                keyExtractor={item => item.id}
                renderItem={({item}) => {
                  let test = '';
                  if (
                    item.profile_path == 'https://image.tmdb.org/t/p/w500null'
                  ) {
                    test =
                      'https://www.tech101.in/wp-content/uploads/2018/07/xblank-profile-picture-300x300.png.pagespeed.ic.vr0p7Y9_y4.webp';
                  } else {
                    test = item.profile_path;
                  }
                  return (
                    <Card style={styles.cardActor}>
                      <Image
                        source={{uri: test}}
                        style={styles.image}
                        resizeMode="contain"
                      />
                      <Text style={styles.name}>{item.name}</Text>
                    </Card>
                  );
                }}
              />
            </View>
          </ScrollView>
        </View>
      )}
    </>
  );
};

export default MovieDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.black,
  },
  header: {
    width: SIZES.width,
    height: SIZES.height * 0.3,
  },
  iconButton: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginHorizontal: 13,
    marginVertical: 13,
  },
  icons2: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  SecSection: {
    marginHorizontal: 13,
  },
  label: {
    marginTop: 18,
    color: COLORS.primary,
    ...FONTS.h2,
    marginBottom: 10,
  },
  secGenre: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  SecSynopsis: {},
  synopsis: {
    ...FONTS.body4,
    color: COLORS.white,
    textAlign: 'justify',
  },
  secActor: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // flexWrap: 'wrap',
  },
  cardActor: {
    height: SIZES.width * 0.29,
    width: SIZES.height * 0.29,
  },

  image: {
    height: '90%',
    width: null,
    resizeMode: 'contain',
  },
  name: {
    ...FONTS.body5,
    color: COLORS.white,
    textAlign: 'center',
    paddingTop: 5,
  },
});
