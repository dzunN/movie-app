/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  Alert,
  FlatList,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ListMovies, PosterMovies} from '../components';
import Loading from '../components/Loading';
import {COLORS, FONTS, SIZES} from '../constants';
import Axios from '../services';

const Home = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [movies, setMovies] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  let isMounted = true;

  function onRefresh() {
    setRefreshing(true);
    getData();
  }

  const setFalse = () => {
    setLoading(false);
    setRefreshing(false);
  };

  async function getData() {
    try {
      await Axios.get('/movies').then((response) => {
        if (isMounted && response.status == 200) {
          setMovies(response.data.results);
        } else {
          Alert.alert('Error to getData');
        }
      });
    } catch (err) {
      console.error(err);
      setFalse();
    } finally {
      setFalse();
    }
  }

  useEffect(() => {
    getData();

    return () => {
      isMounted = false;
    };
  }, []);


  function renderHeader() {
    return (
      <View style={styles.container}>
        {/* Brand */}
        <TouchableOpacity style={styles.button_brand}>
          <Text style={styles.text_brand1}>Movies</Text>
          <Text style={styles.text_brand2}>Portal</Text>
        </TouchableOpacity>
      </View>
    );
  }

  function renderRecomendedMovie() {
    return (
      <FlatList
        horizontal
        pagingEnabled
        snapToAlignment="center"
        snapToInterval={SIZES.width}
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        decelerationRate={0}
        contentContainerStyle={{
          marginTop: SIZES.radius,
        }}
        data={movies}
        keyExtractor={item => `${item.id}`}
        renderItem={({item, index}) => {
          return (
            <PosterMovies
              poster={item.poster_path}
              onPress={() => navigation.navigate('MovieDetail', {id: item.id})}
              key={index}
            />
          );
        }}
      />
    );
  }

  function renderLatestUpload() {
    return (
      <View style={styles.section_lastUp}>
        {/* Header */}
        <View style={{alignItems: 'flex-start'}}>
          <Text style={styles.label}>Latest Upload</Text>
        </View>
        {/* List */}
        {movies.map((item, index) => {
          return (
            <ListMovies
              item={item}
              key={index}
              onPress={() => navigation.navigate('MovieDetail', {id: item.id})}
            />
          );
        })}
      </View>
    );
  }

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <SafeAreaView style={styles.main}>
          <StatusBar barStyle="light-content" backgroundColor={COLORS.black}/>
          <ScrollView
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
            contentContainerStyle={{
              paddingBottom: 10,
            }}>
            {renderHeader()}
            {renderRecomendedMovie()}
            {/* {renderDots()} */}
            {renderLatestUpload()}
          </ScrollView>
        </SafeAreaView>
      )}
    </>
  );
};

export default Home;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: COLORS.black,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZES.padding,
  },
  button_brand: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    height: 50,
  },
  text_brand1: {
    ...FONTS.h1,
    color: COLORS.primary,
    marginRight: 5,
  },
  text_brand2: {
    ...FONTS.h1,
    color: COLORS.white,
  },
  section_lastUp: {
    marginTop: SIZES.body1,
    paddingHorizontal: SIZES.padding,
  },
  label: {
    flex: 1,
    color: COLORS.white,
    ...FONTS.h1,
  },
});
