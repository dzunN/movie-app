import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://code.aldipee.com/api/v1',
});

export default instance;
