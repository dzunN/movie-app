import React, {useEffect, useState} from 'react';
import {MovieDetail, Home, Splash} from './screens';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import NetInfo from '@react-native-community/netinfo';
import {NoInternet} from './components';

const Stack = createNativeStackNavigator();

const App = () => {
  const [isOffline, setOfflineStatus] = useState(false);

  const removeNetInfo = () => {
    NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  }, []);

  return (
    <NavigationContainer>
      {isOffline ? (
        <NoInternet />
      ) : (
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
          initialRouteName={'Home'}>
          <Stack.Screen name="Splash" component={Splash} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="MovieDetail" component={MovieDetail} />
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

export default App;
